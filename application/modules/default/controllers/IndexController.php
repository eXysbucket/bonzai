<?php

class IndexController extends Zend_Controller_Action {
    /*
      public function preDispatch()
      {
      if (!Zend_Auth::getInstance()->hasIdentity())
      $this->_helper->redirector('index','identification','default');
      }
     */

    function indexAction() {
        $this->view->title = "Product list";
        $products = new Model_Product();
        $this->view->products = $products->getMapper()->fetchAll();
       
    }

    function addAction() {
        $this->view->title = "Add a product";
        $form = new Forms_FormProduct();
        $form->submit->setLabel('Add');
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();
            if (!empty($formData) && $form->isValid($formData)) {
                $products = new Model_Product();
                $row = $products->getMapper()->getDbTable()->createRow();

                $row->name = $form->getValue('name');
                $row->id_cat = $form->getValue('id_cat');

                $row->save();
                $this->_redirect('/index/');
            } else {
                $form->populate($formData);
            }
        }
    }

    function deleteAction() {
        if ($this->_request->isPost()) {
            $ids = $this->_request->getParam('check', 0);
            if (!empty($ids)) {
                $products = new Model_Product();
                $products->getMapper()->delete(array_keys($ids));
                $this->_redirect('/index/');
            } else {
                $this->_redirect('/index/');
            }
        }
    }

}