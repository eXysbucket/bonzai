<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initAutoload() {
        // Add autoloader empty namespace
        $this->getApplication()
                ->getAutoloader()
                ->pushAutoloader(new \Composer\Autoload\ClassLoader());

        $resourceloader = new Zend_Loader_Autoloader_Resource(array(
            'basePath' => APPLICATION_PATH,
            'namespace' => '',
            'resourceTypes' => array(
                'model' => array(
                    'path' => 'models/',
                    'namespace' => 'Model_'
                ),
                'form' => array(
                    'path' => 'models/Forms',
                    'namespace' => 'Forms_'
                )
            ),
        ));
        // Return it so that it can be stored by the bootstrap
        return $resourceloader;
    }

    protected function _initDB() {

        $resource = $this->getPluginResource("db");
        $db = $resource->getDbAdapter();

        return $db;
    }

}
