<?php

class Model_DbTable_Categories extends Zend_Db_Table_Abstract {
    protected $_name = 'categories';
    protected $_primary = 'id';
    
    protected $_referenceMap = array(
        'Products' => array(
	        'columns'           => 'id',
	        'refTableClass'     => 'Model_DbTable_Products',
        ),
    );
}