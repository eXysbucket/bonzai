<?php

class Model_DbTable_Products extends Zend_Db_Table_Abstract {
    protected $_name = 'products';
    protected $_primary = 'id';

    protected $_referenceMap = array(
        'Categories' => array(
	        'columns'           => 'id_cat',
	        'refTableClass'     => 'Model_DbTable_Categories',
                'refColumns'        => 'id'
        ),
    );
}