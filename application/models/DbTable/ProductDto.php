<?php

class Model_DbTable_ProductDto {

    protected $_id;
    protected $_name;
    protected $_id_cat;
    protected $_catName;
    
    function getId() {
        return $this->_id;
    }

    function getName() {
        return $this->_name;
    }

    function getIdcat() {
        return $this->_id_cat;
    }

    function getCatName() {
        return $this->_catName;
    }

    function setId($_id) {
        $this->_id = $_id;
        return $this;
    }

    function setName($_name) {
        $this->_name = $_name;
        return $this;
    }

    function setIdcat($_id_cat) {
        $this->_id_cat = $_id_cat;
        return $this;
    }

    function setCatName($_catName) {
        $this->_catName = $_catName;
        return $this;
    }
}
