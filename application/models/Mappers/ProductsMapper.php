<?php

class Model_Mappers_ProductsMapper {

    /**
     *
     * @var Model_DbTable_Products
     */
    protected $dbTable;

    const TABLE_NAME = 'Model_DbTable_Products';

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null == $this->dbTable) {
            $this->setDbTable(self::TABLE_NAME);
        }
        return $this->dbTable;
    }

    /**
     * 
     * @return array(Model_DbTable_ProductDto)
     */
    public function fetchAll() {
        $products = $this->getDbTable()->fetchAll();
        $return = array();
        foreach ($products as $value) {
            $productDto = new Model_DbTable_ProductDto();
            $productDto->setId($value->id);
            $productDto->setIdcat($value->id_cat);
            $productDto->setName($value->name);
            
            $cat = new Model_DbTable_Categories();
            $rowcat = $cat->fetchRow(
                            $cat->select()
                                ->where("id = ?",$value->id_cat));
            
            $productDto->setCatName($rowcat->name);
            array_push($return, $productDto);
        }
        return $return;
    }

    public function delete($ids) {
        $where = $this->getDbTable()->getAdapter()->quoteInto("id IN (?)", $ids);
        return $this->getDbTable()->delete($where);
    }
}
