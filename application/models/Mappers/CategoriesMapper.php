<?php

class Model_Mappers_CategoriesMapper {
    /**
     *
     * @var Model_DbTable_Categories
     */
    protected $dbTable;
    const TABLE_NAME = 'Model_DbTable_Categories';
    
    public function setDbTable($dbTable) {
        if(is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if(!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if(null == $this->dbTable) {
            $this->setDbTable(self::TABLE_NAME);
        }
        return $this->dbTable;
    }
}