<?php

class Model_Product extends Model_Abstract {

    /**
     *
     * @var Model_Mappers_ProductsMapper 
     */
    protected $_mapper;
    const MAPPER_CLASS = 'Model_Mappers_ProductsMapper';

    public function setMapper($mapper) {
        $this->_mapper = new $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(self::MAPPER_CLASS);
        }
        return $this->_mapper;
    }

}
