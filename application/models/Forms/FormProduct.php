<?php

class Forms_FormProduct extends Zend_Form
{
    public function __construct($options = null)
    {
        parent::__construct($options);

        $name = new Zend_Form_Element_Text('name');
        $name->setLabel("Name")
        ->setRequired(true)
        ->addFilter('StripTags')
        ->addFilter('StringTrim')
        ->addValidator('NotEmpty');

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id', 'submitbutton');
		
        $cats = new Zend_Form_Element_Select('id_cat');
        $cats->setLabel('Categories')
                 ->setRequired(true);

        $table = new Model_Mappers_CategoriesMapper();
        foreach($table->getDbTable()->fetchAll() as $c) {
                $cats->addMultiOption($c->id, $c->name);
        }

        $this->addElements(array($name, $cats, $submit));
    }
}